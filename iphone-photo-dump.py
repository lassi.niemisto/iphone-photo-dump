from win32com.shell import shell, shellcon
from win32com.propsys import propsys
from datetime import datetime
import pythoncom
import os
import time
import argparse

INTERESTING_EXTENSIONS = [".jpg", ".mov", ".heic", ".mp4"]
DATE_PROP_KEY = propsys.PSGetPropertyKeyFromName("System.DateModified")
DATE_ARG_PARSE_STR = '%Y-%m-%d'
DATE_PROP_PARSE_STR = '%Y/%m/%d:%H:%M:%S.%f' # not sure bout the f modifier but it does not really matter
older_than_datetime = None

def recurse_and_get_ishellfolder(base_ishellfolder, path):
    splitted_path = path.split("\\", 1)

    for pidl in base_ishellfolder:
        if base_ishellfolder.GetDisplayNameOf(pidl, shellcon.SHGDN_NORMAL) == splitted_path[0]:
            break

    folder = base_ishellfolder.BindToObject(pidl, None, shell.IID_IShellFolder)

    if len(splitted_path) > 1:
        # More to recurse
        return recurse_and_get_ishellfolder(folder, splitted_path[1])
    else:
        return folder


def move_files(args):
    main_folder = recurse_and_get_ishellfolder(shell.SHGetDesktopFolder(), args.input)

    for photo_folder_pidl in main_folder:
        folder_name = main_folder.GetDisplayNameOf(photo_folder_pidl, shellcon.SHGDN_NORMAL)
        folder = main_folder.BindToObject(photo_folder_pidl, None, shell.IID_IShellFolder)
        for pidl in folder:
            child_name = folder.GetDisplayNameOf(pidl, shellcon.SHGDN_NORMAL)
            ext_lower = os.path.splitext(child_name)[1].lower()

            if ext_lower in INTERESTING_EXTENSIONS:
                file_mod_date = getmodified_datetime_by_pidl(folder, pidl)
                if not older_than_datetime or file_mod_date < older_than_datetime:
                    print("Transferring file: " + child_name)
                    move_file_by_pidl(args.output, folder, pidl, child_name, folder_name + "_")
                else:
                    print("Skipping too recent file: " + child_name)


def move_file_by_pidl(dest_dir, src_ishellfolder, src_pidl, src_filename, name_prefix):
    filename = name_prefix + src_filename  # Avoid conflicts
    dest_fullpath = dest_dir + os.sep + filename
    tries = 2
    i = 1
    while True:
        res = move_file_by_pidl_to_path(src_ishellfolder, src_pidl, dest_dir, filename)
        if res:
            if not os.path.isfile(dest_fullpath):
                print(" -> Move operation returned ok but file did not appear")
            break
        if i < tries:
            i += 1
            time.sleep(3)
        else:
            print(" -> Failed to transfer " + src_filename)
            break


def getmodified_datetime_by_pidl(src_ishellfolder, src_pidl):
    fidl = shell.SHGetIDListFromObject(src_ishellfolder)  # Grab the PIDL from the folder object
    si = shell.SHCreateShellItem(fidl, None, src_pidl)  # Create a ShellItem of the source file
    ps = propsys.PSGetItemPropertyHandler(si)
    date_str = ps.GetValue(DATE_PROP_KEY).ToString()
    return datetime.strptime(date_str, DATE_PROP_PARSE_STR)


def move_file_by_pidl_to_path(src_ishellfolder, src_pidl, dst_path, dst_filename):
    pidl_folder_dst, flags = shell.SHILCreateFromPath(dst_path, 0)
    dst_ishellfolder = shell.SHGetDesktopFolder().BindToObject(pidl_folder_dst, None, shell.IID_IShellFolder)

    fidl = shell.SHGetIDListFromObject(src_ishellfolder)  # Grab the PIDL from the folder object
    didl = shell.SHGetIDListFromObject(dst_ishellfolder)  # Grab the PIDL from the folder object

    si = shell.SHCreateShellItem(fidl, None, src_pidl)  # Create a ShellItem of the source file
    dst = shell.SHCreateItemFromIDList(didl)

    pfo = pythoncom.CoCreateInstance(shell.CLSID_FileOperation, None, pythoncom.CLSCTX_ALL, shell.IID_IFileOperation)
    pfo.SetOperationFlags(shellcon.FOF_NOCONFIRMATION | shellcon.FOF_SILENT | shellcon.FOF_NOERRORUI)
    pfo.MoveItem(si, dst, dst_filename) # Schedule an operation to be performed
    pfo.PerformOperations()
    return not pfo.GetAnyOperationsAborted()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--older-than", help="Only move photos older than YYYY-MM-DD")
    parser.add_argument("-i", "--input", help="input path, e.g. \"This PC\Apple iPhone\Internal Storage\DCIM\"", required=True)
    parser.add_argument("-o", "--output", help="output directory, must exist", required=True)
    args = parser.parse_args()
    if not args.input:
        print("You must specify input path pointing to iPhone photos")
        exit(-1)
    if not args.output:
        print("You must specify output path")
        exit(-1)
    if not os.path.isdir(args.output):
        print("Output path does not exist: " + args.output)
        exit(-1)
    if args.older_than:
        print("Only moving files older than " + args.older_than)
        older_than_datetime = datetime.strptime(args.older_than, DATE_ARG_PARSE_STR)

    move_files(args)
