# Iphone photo dump

Python tool for moving all photos and similar files from iPhone to a Windows machine. Moves photos one by one and keeps on going even if there are any errors in moving specific files. Supports moving "older than" files only.

## Information
* This should work if iPhone DCIM folder appears in Windows Explorer like:
  * ![img_explorer_view.png](img_explorer_view.png)
* If the device (phone) becomes unreachable during transfer, check https://winhelponline.com/blog/error-0x80070141-transferring-photos-iphone-to-pc/

## Installation
Python 3 required, requirements not documented for a fresh setup

## Usage
Connect iPhone, make sure the DCIM folder is accessible and then run

`python iphone-photo-dump.py -i "This PC\Apple iPhone\Internal Storage\DCIM" -o "C:\\pics\\" --older-than 2020-01-02`

## Contributing
May accept good merge requests :)

## License
MIT license

## Project status
2022 one evening coding attempt with successful results